const express = require("express");
const User = require("../models/User");

const router = express.Router();

const createRouter = () => {

  // ======= добавление нового юзера в бд =======
  
  router.post("/", (req, res) => {
    const user = new User(req.body);
    user
      .save()
      .then(user => res.send(user))
      .catch(error => res.sendStatus(400).send(error));
  });

  // =======  проверка введенного логина и пароля =======
  
  router.post('/sessions', async (req, res) => {

    const user = await User.findOne({username: req.body.username});
    if (!user) return res.status(400).send({error: 'Username not found!'});

    const isMatch = await user.checkPassword(req.body.password);
    if (!isMatch) return res.status(400).send({error: 'Password incorrect'});

    user.generateToken();
    await user.save();

    res.send({message: 'Username and password correct!', user});
  });

  return router;
};

module.exports = createRouter;
