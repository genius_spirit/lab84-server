const express = require("express");
const Task = require('../models/Task');
const auth = require('../middleware/auth');

const router = express.Router();

const createRouter = () => {

  router.post('/', auth, (req, res) => {
    req.body.user = req.user._id;
    const task = new Task(req.body);
    task.save()
    .then(result => res.send(result))
    .catch(error => res.send(error));
  });

  router.get('/', auth, (req, res) => {
    Task.find({user: req.user._id})
    .then(result => res.send(result))
    .catch(error => res.send(error));
  });

  router.put('/:id', auth, (req, res) => {
    const id = req.params.id;
    const data = req.body;
    Task.findOne({_id: id}).then(result => {
      if (!req.user._id.equals(result.user)) {
        res.sendStatus(403);
      } else {
        result.set({title: data.title, description: data.description, status: data.status});
        result.save()
        .then(result => res.send(result))
        .catch(error => res.send(error));
      }
    })
  });

  router.delete('/:id', auth, (req, res) => {
    const id = req.params.id;
    Task.findOne({_id: id}).then(result => {
      if (!req.user._id.equals(result.user)) {
        res.sendStatus(403);
      } else {
        Task.findByIdAndRemove(id, (err, doc) => {
          err ? res.status(404) : res.send({ id: doc._id})
        }).catch(err => res.status(500).send(err));
      }
    })
  });

  return router;
};

module.exports = createRouter;